#
# Makefile
# leveck, 2022-05-17 20:12
#

install:
	cp dhuuvral /usr/local/bin
	cp gcat /usr/local/bin

uninstall:
	rm /usr/local/bin/dhuuvral
	rm /usr/local/bin/gcat

# vim:ft=make
#
