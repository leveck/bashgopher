# dhuuvral (formerly bashgopher)

Simple gopher, gemini, and http(s) client written in bash.

Gopher protocol uses curl to download the resource. Gemini protocol uses gcat (included in this repo along with its license), and http(s) uses whatever the $http variable is set to (defaults to lynx).

Very much a work in progress. You are likely to find many issues with the gemini functionality.

